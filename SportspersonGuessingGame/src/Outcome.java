import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

public class Outcome extends SportspersonGUI {

	protected JLabel outcome;
	protected JButton playAgain;
	protected Integer score, totalNoGames;
	
	@Override
	protected void initialSetUp() {
		this.setSize(550, 120);
		// the text for the JLabel will be set in the subclasses
		outcome = new JLabel();
	}
	
	/**
	 * Method to create and add the play again button to the main panel
	 */
	protected void addPlayAgainButton() {
		playAgain = new JButton("Play Again");
		playAgain.addActionListener(this);
		mainPanel.add(playAgain, BorderLayout.SOUTH);
	}
	
	/**
	 * Method to restart the game, sending the current score and total number of
	 * games played to the new instance of LevelOption
	 */
	protected void restartGame() {
		// close the current window and display the new one
		this.dispose();
		LevelOption startAgain = new LevelOption(score, totalNoGames);
		startAgain.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == playAgain) {
			this.restartGame();
		}
	}

}
