import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;

public class ModerateGame extends Level {
	
	private JButton choice0 = new JButton();
	private JButton choice1 = new JButton();
	private JButton choice2 = new JButton();
	
	protected void initialSetUp() {
		super.initialSetUp();
		// set the size of the moderate level screen
		this.setSize(600,450);
		// initialise the playerCalculations variable
		playerCalculations = new PlayerCalculations("moderate");
		// create an array list of JButtons for the three player choices in the moderate game
		choices = new ArrayList<JButton>(Arrays.asList(choice0, choice1, choice2));
	}
	
	/**
	 * @param score - Integer holding score throughout session
	 * @param totalNoGames - Integer holding number of games played in the session
	 */
	public ModerateGame(Integer score, Integer totalNoGames) {
		this.initialSetUp();
		this.score = score;
		this.totalNoGames = totalNoGames;
		// most of initial set up is already done - add two player gridlayout to player panel
		players.setLayout(new GridLayout(2,3));
		// add players to the grid
		this.addPlayers();
		// add the mainPanel to the JFrame
		this.add(mainPanel);
	}
	
	public void addPlayers() {
		super.addPlayers();
	}
	
	public void addPlayersToScreen() {
		super.addPlayersToScreen();
	}
	
	/**
	 * Method to calculate the result from the user's choice
	 * @param choice - String holding the choice the user has selected to be the highest scorer. 
	 * This is in the form of choice0, choice1, choice2 relating to the pressed button.
	 */
	public void calculateResult(String choice) {
		// get the last char in the choice string 
		String substring = choice.substring(choice.length()-1);
		if(substring.equals("0")) {
			userChoice = chosenPlayers.get(0);
		}
		else if(substring.equals("1")) {
			userChoice = chosenPlayers.get(1);
		}
		else {
			userChoice = chosenPlayers.get(2);
		}
		// determine the outcome
		boolean outcome = playerCalculations.calculateResult(chosenPlayers, userChoice);
		// if the user was correct, increment the score
		if(outcome) {
			this.score++;
		}
		// increment the total number of games regardless of outcome 
		this.totalNoGames++;
		this.showOutcomeScreen(outcome);
	}
	
	public void showOutcomeScreen(boolean outcome) {
		super.showOutcomeScreen(outcome);
	}

	/**
	 * Action Performed method to deal with handling of events.
	 * @param ae
	 */
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == choice0) {
			this.calculateResult("choice0");
		}
		else if(ae.getSource() == choice1) {
			this.calculateResult("choice1");
		}
		else if(ae.getSource() == choice2) {
			this.calculateResult("choice2");
		}
	}
}
