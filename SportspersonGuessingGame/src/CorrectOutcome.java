import java.awt.BorderLayout;

public class CorrectOutcome extends Outcome {

	/**
	 * @param userChoice - the Player object that the user has chosen
	 * @param score - the score throughout the session
	 * @param totalNoGames - number of games played throughout the session
	 */
	public CorrectOutcome(Player userChoice, Integer score, Integer totalNoGames) {
		this.initialSetUp();
		this.setSize(350, 100);
		this.score = score;
		this.totalNoGames = totalNoGames;
		// most of initial set up is already done - set the text to display the outcome.
		outcome.setText("<html>You answered correctly with "+userChoice.getFullName()+"!<br> Your"
				+ " score is now "+score+"/"+totalNoGames+".</html>");
		// add the outcome JLabel message to the mainPanel
		mainPanel.add(outcome, BorderLayout.NORTH);
		// add the play again button
		this.addPlayAgainButton();
		// add the mainPanel to the JFrame
		this.add(mainPanel);
	}
	
	protected void initialSetUp() {
		super.initialSetUp();
	}
	
	public void addPlayAgainButton() {
		super.addPlayAgainButton();
	}
}
