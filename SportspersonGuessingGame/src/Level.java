import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Level extends SportspersonGUI {

	protected JLabel aim;
	protected JPanel players;
	protected PlayerCalculations playerCalculations;
	protected Player userChoice = null;
	protected ArrayList<Player> chosenPlayers = new ArrayList<Player>();
	protected ArrayList<JButton> choices;
	protected Integer score, totalNoGames;
	
	@Override
	protected void initialSetUp() {
		aim = new JLabel("Guess the player with the highest score");
		// add the aim of the game to the north of the main panel
		mainPanel.add(aim, BorderLayout.NORTH);
		players = new JPanel();
	}
	
	/**
	 * Method to populate chosenPlayers with random players dependent on the level
	 */
	protected void addPlayers() {
		// get random players for the game 
		chosenPlayers = playerCalculations.getRandomPlayers();

		// add the players to the screen
		this.addPlayersToScreen();
		mainPanel.add(players, BorderLayout.CENTER);
	}
	
	/**
	 * Method to populate the grid layout with the players images and names in the correct order
	 */
	protected void addPlayersToScreen() {
		// set the pictures
		for(int i = 0; i < chosenPlayers.size(); i++) {
			JLabel picLabel = new JLabel(new ImageIcon(chosenPlayers.get(i).getImage()));
			players.add(picLabel);
		}
		// set the names as text on the buttons and add action listeners
		for(int i = 0; i < chosenPlayers.size(); i++) {
			choices.get(i).setText(chosenPlayers.get(i).getFullName());
			choices.get(i).addActionListener(this);
			players.add(choices.get(i));
		}
	}
	
	protected void calculateResult(String choice) {
		// to be completed in subclasses
	}
	
	/**
	 * Method to determine and show the relevant outcome screen
	 * @param outcome - true means the user was correct, false means incorrect
	 */
	protected void showOutcomeScreen(boolean outcome) {
		Outcome outcomeScreen;
		if(outcome) {
			// the guess was correct
			outcomeScreen = new CorrectOutcome(userChoice, score, totalNoGames);
		}
		else {
			// the guess was incorrect
			outcomeScreen = new IncorrectOutcome(userChoice, playerCalculations.getCorrectChoice(), score, totalNoGames);
		}
		outcomeScreen.setVisible(true);
		this.dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		// to be completed in the subclasses
	}
}
