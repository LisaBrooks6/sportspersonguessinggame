import java.awt.image.BufferedImage;

public class Player {
	private String firstName, lastName, fullName, profilePicURL;
	private int id;
	private double points;
	private BufferedImage image;
	
	/**
	 * @param firstName
	 * @param lastName
	 * @param fullName
	 * @param id
	 * @param points
	 * @param profilePicURL
	 */
	public Player(String firstName, String lastName, String fullName, int id, double points, String profilePicURL) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.id = id;
		this.points = points;
		this.profilePicURL = profilePicURL;
	}
	
	/**
	 * Accessor method for firstName
	 * @return firstName - first name of player
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Mutator method for firstName
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Accessor method for lastName
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Mutator method for lastName
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Accessor method for fullName
	 * @return fullName
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Mutator method for fullName
	 * @param fullName
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Accessor method for id
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Mutator method for id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Accessor method for points
	 * @return points
	 */
	public double getPoints() {
		return points;
	}
	/**
	 * Mutator method for points
	 * @param points
	 */
	public void setPoints(double points) {
		this.points = points;
	}
	
	/**
	 * Accessor method for profilePicURL
	 * @return profilePicURL
	 */
	public String getProfilePicURL() {
		return profilePicURL;
	}
	/**
	 * Mutator method for profilePicURL
	 * @param profilePicURL
	 */
	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}
	
	/**
	 * Accessor method for image
	 * @return image
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	/**
	 * Mutator method for image
	 * @param image
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}
}
