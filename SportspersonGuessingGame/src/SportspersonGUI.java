import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public abstract class SportspersonGUI extends JFrame implements ActionListener {
	protected JFrame mainFrame = new JFrame();
	protected JPanel mainPanel = new JPanel();
	
	public SportspersonGUI() {
		this.setSize(500,500);
		this.setLocation(100, 100);
		this.setTitle("Sportsperson Guessing Game!");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}	
	
	protected abstract void initialSetUp();
	public abstract void actionPerformed(ActionEvent ae);
}
