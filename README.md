# README #

### What is this repository for? ###

* This is a game for guessing the highest score between sports players.

### How do I get set up? ###
* **To run the program:**
* Clone the repository
* Go to the directory containing the cloned git repository
* On Mac command line : 
* cd SportspersonGuessingGame
* java -jar SportspersonGuessingGame.jar
* Or navigate to the SportspersonGuessingGame folder and double click the jar file SportspersonGuessingGame.jar

* **To run the tests:**
* import the project into Eclipse IDE 
* run the test classes using JUnit

### JARs ###
* In compiling the program I have used json-simple.jar for the file parsing, and junit-4.10.jar for the unit tests.

### UML Diagram ###
* A UML diagram is included as a .png or .pdf called SportspersonGuessingGameUML.

### Who do I talk to? ###

* Lisa Brooks (brookslisa224@gmail.com)