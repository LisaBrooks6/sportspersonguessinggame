import java.awt.BorderLayout;

public class IncorrectOutcome extends Outcome {

	/**
	 * @param userChoice - the Player object which is the user's choice
	 * @param correctChoice - the Player object which is the true highest scorer
	 * @param score - score throughout the session
	 * @param totalNoGames - number of games played in the session
	 */
	public IncorrectOutcome(Player userChoice, Player correctChoice, Integer score, Integer totalNoGames) {
		this.initialSetUp();
		this.score = score;
		this.totalNoGames = totalNoGames;
		// set the text for the JLabel to inform the user of the outcome.
		outcome.setText("<html>You answered incorrectly! "+userChoice.getFullName()+""
				+ " scored "+userChoice.getPoints()+" points!<br>"
						+ "The highest scorer was "+correctChoice.getFullName()+" with "
								+ ""+correctChoice.getPoints()+" points.<br> Your score is "+score+"/"+totalNoGames+". Better luck next "
										+ "time!</html>");
		// add the outcome Jlabel message to the main panel
		mainPanel.add(outcome, BorderLayout.NORTH);
		// add the play again button
		this.addPlayAgainButton();
		// add the mainPanel to the JFrame
		this.add(mainPanel);
	}
	
	protected void initialSetUp() {
		super.initialSetUp();
	}
	
	public void addPlayAgainButton() {
		super.addPlayAgainButton();
	}
}
