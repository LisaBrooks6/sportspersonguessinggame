import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;

public class DifficultGame extends Level {
	
	private JButton choice0 = new JButton();
	private JButton choice1 = new JButton();
	private JButton choice2 = new JButton();
	private JButton choice3 = new JButton();
	
	/**
	 * @param score - Integer holding the score in the session
	 * @param totalNoGames - Inetger holding the total games played in the session
	 */
	public DifficultGame(Integer score, Integer totalNoGames) {
		this.initialSetUp();
		this.score = score;
		this.totalNoGames = totalNoGames;
		// most of initial set up is already done - add 4 player gridlayout to player panel
		players.setLayout(new GridLayout(2,4));
		// add players to the grid
		this.addPlayers();
		// add the mainPanel to the JFrame
		this.add(mainPanel);
	}
	
	protected void initialSetUp() {
		super.initialSetUp();
		// set the size of the difficult level screen
		this.setSize(800,450);
		// initialise the playerCalculations variable
		playerCalculations = new PlayerCalculations("difficult");
		// create an array list of JButtons for the four player choices in the difficult game
		choices = new ArrayList<JButton>(Arrays.asList(choice0, choice1, choice2, choice3));
	}
	
	public void addPlayers() {
		super.addPlayers();
	}
	
	public void addPlayersToScreen() {
		super.addPlayersToScreen();
	}
	
	/**
	 * Method to calculate the result from the user's choice
	 * @param choice - String holding the choice the user has selected to be the highest scorer. 
	 * This is in the form of choice0, choice1, choice2, choice3 relating to the pressed button.
	 */
	public void calculateResult(String choice) {
		/* get the last char in the choice string to determine the
		 * position of the player in the chosenPlayers arraylist 
		 */
		String substring = choice.substring(choice.length()-1);
		if(substring.equals("0")) {
			userChoice = chosenPlayers.get(0);
		}
		else if(substring.equals("1")) {
			userChoice = chosenPlayers.get(1);
		}
		else if(substring.equals("2")) {
			userChoice = chosenPlayers.get(2);
		}
		else {
			userChoice = chosenPlayers.get(3);
		}
		// get the outcome
		boolean outcome = playerCalculations.calculateResult(chosenPlayers, userChoice);
		// increment the score if the user was correct
		if(outcome) {
			this.score++;
		}
		// increment the total number of games regardless of outcome 
		this.totalNoGames++;
		this.showOutcomeScreen(outcome);
	}
	
	public void showOutcomeScreen(boolean outcome) {
		super.showOutcomeScreen(outcome);
	}
	
	/**
	 * Action Performed method to deal with handling of events.
	 * @param ae
	 */
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == choice0) {
			this.calculateResult("choice0");
		}
		else if(ae.getSource() == choice1) {
			this.calculateResult("choice1");
		}
		else if(ae.getSource() == choice2) {
			this.calculateResult("choice2");
		}
		else if(ae.getSource() == choice3) {
			this.calculateResult("choice3");
		}
	}
}
