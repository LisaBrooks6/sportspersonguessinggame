import java.util.ArrayList;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class EasyGameTests {

	private static PlayerCalculations playerCalculations;
	private static ArrayList<Player> chosenPlayers;
	private Player highestScorer = null;
	private Player lowerScorer = null;

	@BeforeClass
	/**
	 * Set the game level and check that the correct number of players 
	 * have populated chosenPlayers.
	 */
	public static void setUp() {
		playerCalculations = new PlayerCalculations("easy");
		// test chosenPlayers 
		chosenPlayers = playerCalculations.getRandomPlayers();
		// as it is an easy game the size of chosenPlayers should be 2
		Assert.assertTrue(chosenPlayers.size()==2);
	}

	@Test
	/**
	 * Test to check that the file is loading as expected and allPlayers is not empty
	 */
	public void testFileLoading() {
		/*
		 * Test to check that the file is properly loading and is not empty
		 */
		ArrayList<Player> allPlayers = playerCalculations.getAllPlayers();
		// ensure that allPlayers is not empty
		Assert.assertFalse(allPlayers.isEmpty());
	}

	@Test
	/**
	 * Test to check that the correct outcome is reached by playerCalculations.calculateResult method
	 */
	public void testOutcome() {
		// get the highest and lower scorers in chosenPlayers
		this.createTestScenario();
		// test that the outcome was true (correct)
		Assert.assertTrue(playerCalculations.calculateResult(chosenPlayers, highestScorer));
		// test that the outcome was false (incorrect)
		Assert.assertFalse(playerCalculations.calculateResult(chosenPlayers, lowerScorer));

	}

	/**
	 * Create a scenario which can be used for testing if the correct outcome was reached.
	 * The scenario will include one highest scorer and one lower scorer.
	 */
	public void createTestScenario() {
		double points = 0.0;
		for(int player = 0; player < chosenPlayers.size(); player++) {
			if(chosenPlayers.get(player).getPoints() > points) {
				points = chosenPlayers.get(player).getPoints();
				if(highestScorer == null) {
					// first time it has been set
					highestScorer = chosenPlayers.get(player);
				}
				else {
					// it has been set, therefore the previous highestScorer is now the lowerScorer
					lowerScorer = highestScorer;
					highestScorer = chosenPlayers.get(player);
				}
			}
			else {
				lowerScorer = chosenPlayers.get(player);
			}
		}
	}
}
