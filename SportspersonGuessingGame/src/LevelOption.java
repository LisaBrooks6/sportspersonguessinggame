import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class LevelOption extends SportspersonGUI {

	private JPanel levelPanel;
	private JRadioButton easy, moderate, difficult;
	private ButtonGroup levelOptions;
	private JButton play;
	private Integer score, totalNoGames;
	
	/**
	 * @param score - score throughout the session
	 * @param totalNoGames - total number of games played in the session
	 */
	public LevelOption(Integer score, Integer totalNoGames) {
		this.score = score;
		this.totalNoGames = totalNoGames;
		this.initialSetUp();
	}
	
	@Override
	protected void initialSetUp() {
		this.setSize(400,120);
		this.mainPanel.setLayout(new BorderLayout());
		// change the title
		this.setTitle("Sportsperson Guessing Game - Select a level of play");
		// add description to mainPanel
		this.addScorePanel();
		// add the radio button options to the main panel
		this.instantiateRadioButtons();
		// add the play button to the mainPanel
		this.addPlayButton();
		// add the main panel to the JFrame
		this.add(mainPanel);
	}
	
	/**
	 * Method to add the score panel
	 */
	public void addScorePanel() {
		JPanel scorePanel = new JPanel();
		scorePanel.add(new JLabel("Score: "+score+" / "+totalNoGames), BorderLayout.CENTER);
		mainPanel.add(scorePanel, BorderLayout.NORTH);
	}
	
	/**
	 * Method to instantiate the JRadioButtons
	 */
	public void instantiateRadioButtons() {
		// create a JRadioButton Group
		levelOptions = new ButtonGroup();
		
		// button instantiation plus adding action listeners
		easy = new JRadioButton("Easy");
		easy.addActionListener(this);
		moderate = new JRadioButton("Moderate");
		moderate.addActionListener(this);
		difficult = new JRadioButton("Difficult");
		difficult.addActionListener(this);
		
		this.addRadioButtons();	
	}
	
	/**
	 * Method to add the radio buttons to the frame
	 */
	public void addRadioButtons() {
		// add to button group
		levelOptions.add(easy);
		levelOptions.add(moderate);
		levelOptions.add(difficult);
		
		// add the buttons to a panel with grid layout
		levelPanel = new JPanel(new GridLayout(1,3));
		levelPanel.add(easy);
		levelPanel.add(moderate);
		levelPanel.add(difficult);
		
		// add the radio group to the main panel
		JPanel centredButtons = new JPanel();
		centredButtons.add(levelPanel, BorderLayout.CENTER);
		mainPanel.add(centredButtons, BorderLayout.CENTER);
	}
	
	/**
	 * Method to add the play button to the mainPanel
	 */
	public void addPlayButton() {
		play = new JButton("Play");
		play.addActionListener(this);
		mainPanel.add(play, BorderLayout.SOUTH);
	}
	
	/**
	 * Method to determine which level of play the user has selected
	 */
	public void goToLevel() {
		Level level = null;
		if(levelOptions.getSelection() != null) {
			if(easy.isSelected()) {
				level = new EasyGame(score, totalNoGames);
			}
			else if(moderate.isSelected()) {
				level = new ModerateGame(score, totalNoGames);
			}
			else {
				level = new DifficultGame(score, totalNoGames);
			}
			level.setVisible(true);
			this.dispose();
		}
		else {
			JOptionPane.showMessageDialog(this, "Please choose a level of play!", "Input Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	/**
	 * Action Performed method to handle events
	 * @param ae
	 */
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == play) {
			this.goToLevel();
		}
	}
}
