import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ModerateGameTests {
	private static PlayerCalculations playerCalculations;
	private static ArrayList<Player> chosenPlayers;

	// instance vars for test case scenario
	private Player highestScorer = null;
	private ArrayList<Player> lowerScorers = new ArrayList<Player>();

	@BeforeClass
	/**
	 * Set the game level and check that the correct number of players 
	 * have populated chosenPlayers.
	 */
	public static void setUp() {
		playerCalculations = new PlayerCalculations("moderate");
		// test chosenPlayers 
		chosenPlayers = playerCalculations.getRandomPlayers();
		// as it is an easy game the size of chosenPlayers should be 3
		Assert.assertTrue(chosenPlayers.size()==3);
	}

	@Test
	/**
	 * Test to check that the correct outcome is reached by playerCalculations.calculateResult method
	 */
	public void testOutcome() {
		// get the highest and lower scorers in the chosenPlayers
		this.createTestScenario();
		// test that the outcome was true (correct)
		Assert.assertTrue(playerCalculations.calculateResult(chosenPlayers, highestScorer));
		// test that the outcome was false (incorrect)
		Random rand = new Random();
		Assert.assertFalse(playerCalculations.calculateResult(chosenPlayers, lowerScorers.get(rand.nextInt(lowerScorers.size()))));
	}

	/**
	 * Create a scenario which can be used for testing if the correct outcome was reached.
	 * The scenario will include one highest scorer and two lower scorers.
	 */
	public void createTestScenario() {
		double points = 0.0;
		for(int player = 0; player < chosenPlayers.size(); player++) {
			if(chosenPlayers.get(player).getPoints() > points) {
				points = chosenPlayers.get(player).getPoints();
				if(highestScorer == null) {
					// first time it has been set
					highestScorer = chosenPlayers.get(player);
				}
				else {
					// it has been set, therefore the previous highestScorer is now the lowerScorer
					lowerScorers.add(highestScorer);
					highestScorer = chosenPlayers.get(player);
					// if the highest scorer is in the lowerScorers arraylist, remove it
					if(lowerScorers.contains(highestScorer)) {
						lowerScorers.remove(highestScorer);
					}
				}
			}
			else {
				lowerScorers.add(chosenPlayers.get(player));
			}
		}
	}
}
