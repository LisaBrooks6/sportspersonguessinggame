import java.io.FileReader;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * This class makes use of the json-simple.jar
 * @author lisabrooks
 */

public class FileManager {

	/**
	 * Method which parses the json file, creating Player objects and adding them to
	 * the allPlayers arraylist.
	 * @return allPlayers - arraylist holding all players in the file
	 */
	public ArrayList<Player> parsePlayerOptions() {
		JSONParser jsonParser = new JSONParser();
		ArrayList<Player> allPlayers = new ArrayList<Player>();
		try {
			// parse the json file
			Object obj = jsonParser.parse(new FileReader("data/sportspeople.json"));
			JSONObject jsonObj = (JSONObject)obj;
			
			JSONArray sportspeople = (JSONArray)jsonObj.get("sportspeople");
			// loop through the sportspeople jsonArray creating player objects and adding to allPlayers
			for(int i = 0; i < sportspeople.size(); i++) {
				JSONObject person = (JSONObject)sportspeople.get(i);
				String firstName = person.get("first_name").toString();
				String lastName = person.get("last_name").toString();
				String fullName = person.get("full_name").toString();
				int id = Integer.parseInt(person.get("id").toString());
				double points = Double.parseDouble(person.get("points").toString());
				String profilePicURL = person.get("profile_picture_url").toString();
				
				// create a Player object
			    Player p = new Player(firstName, lastName, fullName, id, points, profilePicURL);
				allPlayers.add(p);
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return allPlayers;
	}
}
