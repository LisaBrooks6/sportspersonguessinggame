import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

public class PlayerCalculations {
	private Player correctChoice = null;
	private String gameLevel;
	private ArrayList<Player> allPlayers;
	
	/**
	 * @param gameLevel - String containing either easy, moderate or difficult
	 */
	public PlayerCalculations(String gameLevel) {
		this.gameLevel = gameLevel;
		// load all players first
		this.allPlayers = this.getAllPlayers();
	}

	/**
	 * Method to get random players for the game
	 * @return chosenPlayers - arraylist containing player objects that have
	 * been selected for the game
	 */
	public ArrayList<Player> getRandomPlayers() {
		ArrayList<Player> chosenPlayers = new ArrayList<Player>();
		int noPlayers = this.getNoPlayers();
		Random r = new Random();
	
		for(int i=0; i < noPlayers; i++) {
			// get a random position for the allPlayers arraylist
			int rand = r.nextInt(allPlayers.size());
			Player p = allPlayers.get(rand);
			// add the player to the chosen list
			chosenPlayers.add(p);
			// remove the player from the arraylist
			allPlayers.remove(p);
		}
		
		// set the chosen players images
		this.setChosenPlayersImages(chosenPlayers);
		return chosenPlayers;
	}
	
	/**
	 * Method to load the images from the URL in the file and then set these 
	 * images in the relevant player instance
	 * @param chosenPlayers - the chosen player instances for the game
	 */
	public void setChosenPlayersImages(ArrayList<Player> chosenPlayers) {
		// set the profile pictures for the chosen players
		for(int i = 0; i <chosenPlayers.size(); i++) {
			BufferedImage img = null;
			try {
				img = ImageIO.read(new URL(chosenPlayers.get(i).getProfilePicURL()));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			chosenPlayers.get(i).setImage(img);
		}
	}
	
	/**
	 * Method to return the number of players in the game 
	 * @return either 2, 3, or 4 dependent on game level
	 */
	public int getNoPlayers() {
		// switch statement to determine how many players are required for each game type		
		if(gameLevel.equals("easy")) {
			return 2;
		}
		else if(gameLevel.equals("moderate")) {
			return 3;
		}
		else {
			// game level is difficult
			return 4;
		}
	}
	
	/**
	 * Method to create an instance of FileManager to parse the json file
	 * @return arraylist containing all player objects
	 */
	public ArrayList<Player> getAllPlayers() {
		FileManager fileMgr = new FileManager();
		return fileMgr.parsePlayerOptions();
	}
	
	/**
	 * Method to calculate the result when a user has made a choice
	 * @param choices - all player choices the user was presented with
	 * @param choice - the player that the user chose
	 * @return outcome - true means that the user was correct, false means
	 * they were incorrect
	 */
	public boolean calculateResult(ArrayList<Player> choices, Player choice) {
		// initialise
		boolean outcome = false;
		double points = 0.0;
		Player highestScorer = null;
		
		// loop through the choices to find the player with the highest score
		for(int i = 0; i < choices.size(); i++) {
			if(choices.get(i).getPoints() > points) {
				points = choices.get(i).getPoints();
				highestScorer = choices.get(i);
			}
		}
		// check if the id of the highest scorer is the same as the id of the choice 
		if(highestScorer != null) {
			if(highestScorer.getId() == choice.getId()) {
				outcome = true;
			}
			setCorrectChoice(highestScorer);
		}
		return outcome;
	}
	
	/**
	 * Mutator method for setting the correctChoice 
	 * @param p - Player who is the correct choice
	 */
	public void setCorrectChoice(Player p) {
		correctChoice = p;
	}
	
	/**
	 * Accessor method for correctChoice 
	 * @return correctChoice
	 */
	public Player getCorrectChoice() {
		return correctChoice;
	}
}

